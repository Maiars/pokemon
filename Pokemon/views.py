from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User

from .models import get_list_pokemon, Pokemon, Inventaire, insert_pokemon_atk, insert_pokemon, MyPokemons, Attaque, \
    Adversaire, Marchand


def index(request):
    tmpl = "Pokemon/index.html"
    return render(request, tmpl, insert_pokemon(), get_list_pokemon(), insert_pokemon_atk())


def menu(request):
    tmpl = "Pokemon/menu.html"
    return render(request, tmpl)


def pokedex(request):
    tmpl = "Pokemon/pokedex.html"
    pokemonss = Pokemon.objects.all()
    return render(request, tmpl, {'pokemonss': pokemonss})


def inscription(request):
    tmpl = "Pokemon/inscription.html"
    return render(request, tmpl)


def inventaire(request):
    tmpl = "Pokemon/inventaire.html"
    this_user = request.user
    NB_items = Inventaire.objects.filter(user_name__pseudo__startswith=this_user).all()
    return render(request, tmpl, {'NB_items': NB_items})


def voyage(request):
    tmpl = "Pokemon/voyage.html"
    return render(request, tmpl)


def combat(request):
    tmpl = "Pokemon/combat.html"
    this_user = request.user
    pokemons = Pokemon.objects.all()
    attaques = Attaque.objects.all()
    adversaires = Adversaire.objects.all()
    mypokemons = MyPokemons.objects.filter(pseudo__pseudo__startswith=this_user).all()[:1]
    return render(request, tmpl,
                  {'mypokemons': mypokemons, 'pokemons': pokemons, 'attaques': attaques, 'adversaires': adversaires}
                  )


def marchand(request):
    tmpl = "Pokemon/marchand.html"
    this_user = request.user
    mon_inventaires = Inventaire.objects.filter(user_name__pseudo__startswith=this_user).all()
    son_inventaires = Inventaire.objects.filter(user_name__pseudo__startswith="Marchand").all()
    return render(request, tmpl, {'mon_inventaires': mon_inventaires, 'son_inventaires': son_inventaires})
