from random import randint

from django.db import models
import requests


class User(models.Model):
    pseudo = models.CharField(max_length=30, null=False)
    password = models.CharField(max_length=150, null=False)

    def __str__(self):
        return self.pseudo


class Attaque(models.Model):
    attaque_name = models.CharField(max_length=150, null=False)
    degat = models.IntegerField(blank=True, default=20)

    def __str__(self):
        return self.attaque_name


class Pokemon(models.Model):
    pokemon_name = models.CharField(max_length=150, null=False)

    def __str__(self):
        return self.pokemon_name


class Adversaire(models.Model):
    name = models.CharField(max_length=150, null=False)
    pokemon_name = models.ManyToManyField(Pokemon)
    vie = models.IntegerField(blank=True, default=100)
    vie_actu = models.IntegerField(blank=True, default=100)
    attaque = models.ManyToManyField(Attaque)
    en_vie = models.BooleanField(default=True)


class Inventaire(models.Model):
    user_name = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True, null=False)
    poke_ball = models.IntegerField(blank=True, null=True)
    super_ball = models.IntegerField(blank=True, null=True)
    hyper_ball = models.IntegerField(blank=True, null=True)
    potion = models.IntegerField(blank=True, null=True)
    super_potion = models.IntegerField(blank=True, null=True)
    hyper_potion = models.IntegerField(blank=True, null=True)
    gold = models.IntegerField(blank=True, null=True)


class Marchand(models.Model):
    pseudo = models.CharField(max_length=30, null=False)
    inventaire = models.OneToOneField(Inventaire, on_delete=models.CASCADE, null=False)


class MyPokemons(models.Model):
    pokemon_name = models.ManyToManyField(Pokemon)
    pseudo = models.ManyToManyField(User)
    vie = models.IntegerField(blank=True, default=100)
    vie_actu = models.IntegerField(blank=True, default=100)
    attaque_name = models.ManyToManyField(Attaque)
    en_vie = models.BooleanField(default=True)


def get_list_pokemon():
    result = requests.get("https://pokeapi.co/api/v2/pokemon").json()['results']

    pokeName = []
    for results in result:
        pokeName.append(results['name'])

    context = {'Pokemons': pokeName}
    return context


def insert_pokemon():
    tab = get_list_pokemon()['Pokemons']
    for tabs in tab:
        insert = Pokemon(pokemon_name=tabs)
        insert.save()


def get_list_atk_pokemon():
    result = requests.get("https://pokeapi.co/api/v2/type/3").json()['moves']

    pokeAtk = []
    for results in result:
        pokeAtk.append(results['name'])

    context = {'PokemonsAtk': pokeAtk}

    return context


def insert_pokemon_atk():
    tab = get_list_atk_pokemon()['PokemonsAtk']
    for tabs in tab:
        insert = Attaque(attaque_name=tabs, degat=randint(1, 50))
        insert.save()


def insert_user(pseudo, email, mdp, mdp2):
    if mdp == mdp2:
        user = User.objects.create_user(pseudo, email, mdp)
        user.last_name = pseudo
        user.save()
