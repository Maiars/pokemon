from django.contrib import admin

# Register your models here.
from Pokemon.models import *

admin.site.register(User)
admin.site.register(Attaque)
admin.site.register(Pokemon)
admin.site.register(MyPokemons)
admin.site.register(Inventaire)
admin.site.register(Adversaire)
admin.site.register(Marchand)