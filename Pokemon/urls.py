from django.contrib.auth.views import LoginView
from django.urls import path

from . import views

urlpatterns = [
    path('', LoginView.as_view()),
    path('inscription/', views.inscription, name="inscription"),
    path('menu/', views.menu, name="menu"),
    path('menu/pokedex/', views.pokedex, name="pokedex"),
    path('menu/inventaire/', views.inventaire, name="inventaire"),
    path('menu/voyage/', views.voyage, name="voyage"),
    path('menu/voyage/combat/', views.combat, name="combat"),
    path('menu/voyage/combat/marchand/', views.marchand, name="marchand"),
]
