# Generated by Django 2.2.6 on 2019-10-30 13:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Pokemon', '0003_auto_20191030_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='pokedex',
            name='user',
            field=models.OneToOneField(default=0, on_delete=django.db.models.deletion.CASCADE, to='Pokemon.User'),
            preserve_default=False,
        ),
    ]
