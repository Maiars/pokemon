# Generated by Django 2.2.7 on 2019-11-13 20:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Pokemon', '0007_auto_20191112_2305'),
    ]

    operations = [
        migrations.CreateModel(
            name='Inventaire',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('poke_ball', models.IntegerField(max_length=1000, null=True)),
                ('super_ball', models.IntegerField(max_length=1000, null=True)),
                ('hyper_ball', models.IntegerField(max_length=1000, null=True)),
                ('potion', models.IntegerField(max_length=1000, null=True)),
                ('super_potion', models.IntegerField(max_length=1000, null=True)),
                ('hyper_potion', models.IntegerField(max_length=1000, null=True)),
                ('gold', models.IntegerField(max_length=100000000, null=True)),
                ('user_name', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='Pokemon.User')),
            ],
        ),
    ]
