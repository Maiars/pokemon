# Generated by Django 2.2.7 on 2019-11-12 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Pokemon', '0005_auto_20191030_1407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pokedex',
            name='pokemon',
        ),
        migrations.AddField(
            model_name='pokedex',
            name='pokemon',
            field=models.ManyToManyField(to='Pokemon.Pokemon'),
        ),
    ]
